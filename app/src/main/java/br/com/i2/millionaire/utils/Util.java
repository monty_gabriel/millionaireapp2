package br.com.i2.millionaire.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.com.i2.millionaire.DetailsActivity;
import br.com.i2.millionaire.Loginctivity;
import br.com.i2.millionaire.MillionActivity;
import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.helper.Preferencias;

import static android.content.Context.MODE_PRIVATE;

public final class Util {
    private static final String TAG = "Util";
    private static Util instance;
    private static FirebaseAuth firebaseAuth;

    private Util() {
    }

    public static synchronized Util getInstance() {
        if (instance == null) instance = new Util();
        return instance;
    }

    public static void home(Context context, Intent intent) {
        intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void rebootApp(Context context, Intent intent) {
        intent = new Intent(context, MillionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void openApp(Context context, Intent intent) {
        intent = new Intent(context, MillionActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static boolean isDeviceScreenLocked(Context context) {
        boolean isLocked = false;

        // First we check the locked state
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        boolean inKeyguardRestrictedInputMode = keyguardManager.inKeyguardRestrictedInputMode();

        if (inKeyguardRestrictedInputMode)
            isLocked = true;
        else {
            // If password is not set in the settings, the inKeyguardRestrictedInputMode() returns false,
            // so we need to check if screen on for this case

            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH)
                isLocked = !powerManager.isInteractive();
            else
                //noinspection deprecation
                isLocked = !powerManager.isScreenOn();
        }

        Log.i(TAG, String.format("Now device is %s.", isLocked ? "locked" : "unlocked"));
        return isLocked;
    }

    public static void unLockScreenDevice(Context context) {
        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km.newKeyguardLock("MyKeyguardLock");
        kl.disableKeyguard();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
        wakeLock.acquire();
    }

    public static void notLockScreenDevice(Activity activity) {
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public static void closeApp(Activity activity) {
        activity.finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public static void generateHashSHA(Context context, String strPackage) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(strPackage, PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public static void logout(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Alerta");
        builder.setMessage("Deseja realmente sair da app?");
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                firebaseAuth = ConfiguracaoFirebase.getFirebaseAuth();
                firebaseAuth.signOut();
                LoginManager.getInstance().logOut();
                activity.startActivity(new Intent(activity, Loginctivity.class));
                activity.finish();

                dialogInterface.dismiss();
                Preferencias preferencias = new Preferencias(activity, "sessionActivityBuyTicket", MODE_PRIVATE);


            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


}