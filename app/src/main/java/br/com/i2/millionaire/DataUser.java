package br.com.i2.millionaire;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.model.User;
import br.com.i2.millionaire.utils.Util;

public class DataUser extends AppCompatActivity {
    private Toolbar toolbar;

    private EditText editName, editLastName, editCPF, editDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_user);

        toolbar = findViewById(R.id.top_toolbar);
        toolbar.setTitle("Dados do Usuário");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editName = findViewById(R.id.editName);
        editLastName = findViewById(R.id.editLastName);
        editCPF = findViewById(R.id.editCPF);
        editDate = findViewById(R.id.editDate);

        SimpleMaskFormatter maskCfp = new SimpleMaskFormatter("NNN.NNN.NNN-NN");
        MaskTextWatcher watcherCpf = new MaskTextWatcher(editCPF, maskCfp);
        editCPF.addTextChangedListener(watcherCpf);

        SimpleMaskFormatter maskDate = new SimpleMaskFormatter("NN/NN/NNNN");
        MaskTextWatcher watcherDate = new MaskTextWatcher(editDate, maskDate);
        editDate.addTextChangedListener(watcherDate);
    }

    private void saveDateUser() {
        String uid = new Preferencias(this).getValue(this.getString(R.string.id_user_app));
        User user = new User();
        user.setId(uid);
        user.setName(editName.getText().toString());
        user.setLastName(editLastName.getText().toString());
        user.setCpf(unimaskCpf(editCPF.getText().toString()));
        user.setDate(unimaskData(editDate.getText().toString()));
        user.setAccountChecked(false);
        user.saveDataUser();

        Intent intent = new Intent(this, CheckAccountActivity.class);
        intent.putExtra("nameUser", user.getName()).
                putExtra("lastNameUser", user.getLastName()).
                putExtra("cpfUser", user.getCpf()).
                putExtra("dateUser", user.getDate());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.item_sair:
                Util.logout(this);
                return true;
        }
        return true;
    }

    public void saveDataUser(View view) {
        if (editName.getText().toString().equals("") && editLastName.getText().toString().equals("") &&
                editCPF.getText().toString().equals("") && editDate.getText().toString().equals("") ||
                ((editName.getText().toString().equals("") || editLastName.getText().toString().equals("") ||
                        editCPF.getText().toString().equals("") || editDate.getText().toString().equals(""))))
            alert("Por favor preencha todos os dados!");
        else if (unimaskCpf(editCPF.getText().toString()).length() < 11)
            alert("CPF Inválido!");
        else if (unimaskData(editDate.getText().toString()).length() < 8)
            alert("Data Inválida!");
        else confirm("Confirma a gravação?");
    }

    private void confirm(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alerta!");
        builder.setMessage(message);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                saveDateUser();
                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void alert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alerta!");
        builder.setMessage(message);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    private String unimaskCpf(String cpf) {
        return cpf.replace(".", " ").replace("-", " ").replace(" ", "");
    }

    private String unimaskData(String data) {
        return data.replace("/", " ").replace(" ", "");
    }
}
