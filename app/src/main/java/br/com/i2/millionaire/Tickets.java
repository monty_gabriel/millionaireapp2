package br.com.i2.millionaire;


public class Tickets {

        private String ticket;


        public Tickets() {}

        public Tickets(String ticket) {
            this.ticket = ticket;

        }

        public String getTicket() {
            return ticket;
        }

        public void setTicket(String ticket) {
            this.ticket = ticket;
        }

}