package br.com.i2.millionaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.model.User;

public class NewUserActivity extends AppCompatActivity {
    //Create Objects
    private EditText editName, editEmail, editPassword;
    private Button btnRegister;
    private User user;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);


        //Instantiating Layout objects bound to this class. The instance is performed according to the identifier of the object (id)
        editName = findViewById(R.id.edit_name);
        editEmail = findViewById(R.id.edit_email);
        editPassword = findViewById(R.id.edit_password);
        btnRegister = findViewById(R.id.btn_register);

       //Action of click Button Register
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user = new User();
                user.setName(editName.getText().toString());
                user.setEmail(editEmail.getText().toString().trim());
                user.setPassword(editPassword.getText().toString());
                if (user.getName().equals("") && user.getEmail().equals("") && user.getPassword().equals("") ||
                        (user.getName().equals("") || user.getEmail().equals("") || user.getPassword().equals("")))
                    Toast.makeText(NewUserActivity.this, "Todos os campos são obrigatórios!", Toast.LENGTH_LONG).show();
                else cadastrarUsuario();
            }
        });
    }


    //Method of register user
    private void cadastrarUsuario() {
        firebaseAuth = ConfiguracaoFirebase.getFirebaseAuth();
        firebaseAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(NewUserActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Preferencias preferencias = new Preferencias(NewUserActivity.this);
                            preferencias.savePreferences(getString(R.string.id_user_app), firebaseAuth.getUid());

                            user.setId(firebaseAuth.getUid());
                            user.setAccountChecked(false);
                            user.saveUser();

                            openHistoricScreen();

                        } else {
                            String erroExececao = "";
                            try {
                                throw task.getException();
                            } catch (FirebaseAuthWeakPasswordException e) {
                                erroExececao = "Digite uma senha mais forte, contendo mais caracteres e com eltras e número!";
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                                erroExececao = "O e-mail digitado é inválido, digite um novo e-mail!";
                            } catch (FirebaseAuthUserCollisionException e) {
                                erroExececao = "Esse e-mail já esta em uso no app!";
                            } catch (Exception e) {
                                erroExececao = "Esse ao efetuar cadastro!";
                            }
                            Toast.makeText(NewUserActivity.this, "Erro ao cadastrar usuário: " + erroExececao, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    //Method of open screen
    private void openHistoricScreen() {
        startActivity(new Intent(this, HistoricActivity.class));
        finish();
    }

    public void backLogin(View view){
        startActivity(new Intent(this, Loginctivity.class));
        finish();
    }
}