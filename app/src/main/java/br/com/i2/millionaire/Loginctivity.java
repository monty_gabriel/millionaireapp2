package br.com.i2.millionaire;

import android.accounts.Account;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.paypal.android.sdk.payments.LoginActivity;
import com.tfb.fbtoast.FBToast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.util.Arrays;

import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.consts.Const;
import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.model.User;

import static br.com.i2.millionaire.consts.Const.RC_SIGN_IN;

public class Loginctivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {


   //Create Objects
    private GoogleApiClient googleApiClient;
    private EditText editEmail, editPassword;
    private Button btnLoginFirebase, btnFacebookLogin, btnGoogle;
    private CallbackManager callbackManager;
    private GoogleSignInClient mGoogleSignInClient, googleSignInClient;
    private TextView createAccount;
    public static final int SIGN_IN_CODE = 77;
    private FirebaseAuth firebaseAuth, mFirebaseAuth;
    private static final String TAG = "SignInActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginctivity);


        //Configurations of Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();


       googleApiClient = new GoogleApiClient.Builder(this)
               .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
               .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
               .build();


        LoginManager.getInstance().logOut();
       //Configurations of Firebase
        mFirebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth = ConfiguracaoFirebase.getFirebaseAuth();







        //Instantiating Layout objects bound to this class. The instance is performed according to the identifier of the object (id)
        createAccount = findViewById(R.id.edit_create_account);
        editEmail = findViewById(R.id.edit_email);
        editPassword = findViewById(R.id.edit_password);
        btnLoginFirebase = findViewById(R.id.btn_firebase_login);
        btnFacebookLogin = findViewById(R.id.btn_facebook_login);
        btnGoogle = findViewById(R.id.btn_google);



        //By activating the "onClick" method, just below the Google button
        btnGoogle.setOnClickListener(this);



        //
        //What will happen when the conventional login button is triggered
        btnLoginFirebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editEmail.getText().toString().trim().equals("") && editPassword.getText().toString().equals("") ||
                        (editEmail.getText().toString().trim().equals("") || editPassword.getText().toString().equals("")))
                    Toast.makeText(Loginctivity.this, "Todos os campos são obrigatórios!", Toast.LENGTH_LONG).show();
                else loginFirebase();
            }
        });


        //What will happen when the  facebook button is triggered
        btnFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginFacebook();
            }
        });


        //What will happen when the Create Account is triggered
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Loginctivity.this, NewUserActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }




    //conventional login method
    private void loginFirebase() {
        firebaseAuth.signInWithEmailAndPassword(editEmail.getText().toString().trim(), editPassword.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful())
                            Log.d(Const.TAG_LOGIN_FIREBASE, "Firebase login error", task.getException());

                        if (task.isSuccessful()) {
                            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                            saveUser(currentUser, currentUser.getEmail());

                        } else
                            FBToast.errorToast(Loginctivity.this,"Authentication Failed !",FBToast.LENGTH_SHORT);
                    }
                });
    }


    //facebook login method
    private void loginFacebook() {
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().logInWithReadPermissions(Loginctivity.this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

                Log.d(Const.TAG_LOGIN_FACEBOOK, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(Const.TAG_LOGIN_FACEBOOK, "facebook:onError", error);
            }
        });
    }
    private void handleFacebookAccessToken ( final AccessToken token){
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                            saveUser(currentUser, null);
                        } else
                            Toast.makeText(Loginctivity.this, "Authentication with Facebook failed.",
                                    Toast.LENGTH_SHORT).show();
                    }
                });
    }


    //Google Login Method
    private void signIn () {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful())
                            Log.d(Const.TAG_LOGIN_GOOGLE, "Google login error", task.getException());

                        if (task.isSuccessful()) {
                            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                            saveUser(currentUser, acct.getEmail());
                        } else
                            Toast.makeText(Loginctivity.this, "Authentication with Google failed.",
                                    Toast.LENGTH_SHORT).show();
                    }
                });
    }


    //Method for open Historic Activity
    private void openHistociScreen() {
        Intent intent = new Intent(Loginctivity.this, HistoricActivity.class);
        startActivity(intent);
        finish();
    }
    //Method of text Create Account .. Open this acitivity
    public void createAccount(View view) {
        startActivity(new Intent(this, NewUserActivity.class));
        finish();
    }


    //Method for save User
    private void saveUser (FirebaseUser firebaseUser, String email){
        FBToast.successToast(Loginctivity.this,"Login efetuado com sucesso !",FBToast.LENGTH_SHORT);
        FirebaseUser currentUser = firebaseUser;
        Preferencias preferencias = new Preferencias(Loginctivity.this);
        preferencias.savePreferences(getString(R.string.id_user_app), firebaseUser.getUid());
        preferencias.savePreferences(getString(R.string.username_app), firebaseUser.getDisplayName());
        if (email != null) preferencias.savePreferences(getString(R.string.email_app), email);

        User user = new User();
        user.setId(firebaseUser.getUid());
        user.setName(currentUser.getDisplayName());
        user.setAccountChecked(false);
        if (email != null)
            user.setEmail(email);
        user.saveUser();
        openHistociScreen();


    }

    //Méthod for check user informations
    private void updateUI ( final GoogleSignInAccount acct){
        if (acct != null) {
            AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
            firebaseAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful())
                                Log.d(Const.TAG_LOGIN_GOOGLE, "Google login error", task.getException());

                            if (task.isSuccessful()) {
                                FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                                saveUser(currentUser, acct.getEmail());
                                openHistociScreen();
                            } else
                                Toast.makeText(Loginctivity.this, "Authentication with Google failed.",
                                        Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Toast.makeText(this, "Errinho qualquer", Toast.LENGTH_SHORT).show();
        }
    }

    //Methods Override
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Const.RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed
                Log.e(TAG, "Google Sign In failed.");
            }
        }
        //Sem essa instrução , o login do facebook não irá funcionar
        else callbackManager.onActivityResult(requestCode, resultCode, data);
    }








    @Override
    public void onConnectionFailed (@NonNull ConnectionResult connectionResult){
            Log.d(TAG, "onConnectionFailed:" + connectionResult);
            FBToast.errorToast(Loginctivity.this,"Google Play Services error.",FBToast.LENGTH_SHORT);
        }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_google:
                signIn();
                break;
        }
    }


    //
    @Override
    public void onStart() {
        super.onStart();
        if (firebaseAuth.getCurrentUser() != null)
            openHistociScreen();


        // Toast.makeText(this, "Seja bem-vindo novamente!", Toast.LENGTH_SHORT).show();

        //GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        //updateUI(account);
    }

    @Override
    public void onStop() {
        super.onStop();
        // if (firebaseAuth.getCurrentUser() != null) openHistociScreen();


    }

    // All Methods








}