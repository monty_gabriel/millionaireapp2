package br.com.i2.millionaire;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.List;

import br.com.i2.millionaire.adapter.DetailsAdapter;
import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.consts.Const;
import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.model.Ticket;
import br.com.i2.millionaire.model.User;
import br.com.i2.millionaire.pojo.Detail;

public class TicketsBuys extends AppCompatActivity {
    private DatabaseReference databaseReference;

    private TextView texto;
    private ListView ticketsUsuario;
    private ArrayList<String> meusTickets = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private ValueEventListener valueEventListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickets_buys);


        //Monty, a intenção é que essa Activity receba os tickets comprados pelo o usuário


        //Cast
        texto = findViewById(R.id.texto);
        ticketsUsuario = findViewById(R.id.lista_tickets);


        //PReferences
        Preferencias preferencias = new Preferencias(TicketsBuys.this);
        final String idUser = preferencias.getValue(getString(R.string.id_user_app));
        //adapter (TicketsBuys.this, android.R.layout.simple_list_item_1, meusTickets);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("tickets").child("tickets_por_usuario").child(idUser);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, meusTickets);
        ticketsUsuario.setAdapter(adapter);

        ValueEventListener eventListenerr = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

               // String message = dataSnapshot.child("tickets").child("tickets_por_usuario").child(idUser).getValue(String.class);
                String comment = dataSnapshot.child("").child("valor").getKey();

               meusTickets.add(comment);
               texto.setText(comment);
                Log.d(Const.LOG_CAT, "Reference : " + databaseReference.toString());
                Log.d(Const.LOG_CAT, "Reference : " + dataSnapshot.getKey());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }


        };
        databaseReference.addListenerForSingleValueEvent(eventListenerr);
    }

}