package br.com.i2.millionaire;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.i2.millionaire.adapter.DetailsAdapter;
import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.pojo.Detail;
import br.com.i2.millionaire.utils.Util;

public class DetailsActivity extends AppCompatActivity {
    private ListView listView;
    private List<Detail> tickets,tickets2;
    private DetailsAdapter arrayAdapter, arrayAdapter2;
    private Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


        //checkBoxBuy = convertView.findViewById(R.id.checkBox);

        toolbar = findViewById(R.id.top_toolbar);
        toolbar.setTitle("Details Tickets");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    static class ViewHolder {
        CheckBox checkBox;
        TextView textView;
    }

    @Override
    protected void onStart() {
        getTicketForUser();
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.item_sair:
                Util.logout(this);
                return true;
        }
        return true;
    }

    private void getTicketForUser() {


        listView = findViewById(R.id.detail_tickets);

        tickets = new ArrayList<>();
        arrayAdapter = new DetailsAdapter(this, tickets);
        listView.setAdapter(arrayAdapter);

        tickets.clear();

        Preferencias preferencias = new Preferencias(DetailsActivity.this, "sessionActivityBuyTicket", MODE_PRIVATE);
        Map<String, ?> prefsMap = preferencias.getAll();
        for (Map.Entry<String, ?> entry : prefsMap.entrySet())
            tickets.add(new Detail(entry.getKey(), entry.getValue().toString()));

        arrayAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onStop() {
        super.onStop();


    }


    }
