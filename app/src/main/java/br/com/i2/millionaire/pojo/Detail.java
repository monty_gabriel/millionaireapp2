package br.com.i2.millionaire.pojo;

import br.com.i2.millionaire.consts.Const;

public class Detail {
    private String ticket;
    private String status;
    private Double value;

    public Detail() {
    }

    public Detail(String ticket, String status) {
        this.ticket = ticket;
        this.status = status;
    }

    public String getTicket() {
        return ticket;
    }

    public void setgetTicket(String value) {
        this.ticket = ticket;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double ticket) {
        if (ticket == null) this.value = Const.Currency.VALUE_TICKET;
        else this.value = value;
    }

    @Override
    public String toString() {
        return ticket + " - " + value + " - " + status;
    }
}
