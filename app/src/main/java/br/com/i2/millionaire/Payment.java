package br.com.i2.millionaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.tfb.fbtoast.FBToast;

import org.json.JSONException;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.utils.Util;

public class Payment extends AppCompatActivity {
    public static final int PAYPAL_REQUEST_CODE = 7171;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .acceptCreditCards(true)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId("Abo0uE94tjge6Laqv3mEyF5qr9xBXjXSQSeaAUKf0_7sEpr8O-2gC43gCMBoPzPjVdxikTZc0RgCJjsO");

    private Button btnPayNow;
    private EditText payAmount;
    private String amount = "";
    private Toolbar toolbar;

    private List<Integer> listNumbers;

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        //Create toolbar
        toolbar = findViewById(R.id.top_toolbar);
        toolbar.setTitle("Pagamento");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);


        //Casts
        btnPayNow = findViewById(R.id.btnPayNow);
        payAmount = findViewById(R.id.editAmount);

        Bundle bundle = getIntent().getExtras();

        payAmount.setText(bundle.get("valorFinalCompra").toString());

        listNumbers = (List<Integer>) bundle.get("listNumbers");

        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processPayment();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.item_sair:
                Util.logout(this);
                return true;
        }
        return true;
    }

    private void processPayment() {
        amount = payAmount.getText().toString().replace("$", "").replace(",", ".");
        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(String.valueOf(amount)), "USD",
                "Donate for Millionaire", PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                FBToast.successToast(this,"Sucess Buy!",FBToast.LENGTH_SHORT);
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirmation != null) {
                    try {
                        String paymentDetails = confirmation.toJSONObject().toString(4);
                        startActivity(new Intent(this,
                                PaymentDetails.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PayAmount", amount)
                                .putExtra("listNumbers", (ArrayList<Integer>) listNumbers));



                        //Preferencias preferencias = new Preferencias(this, "sessionActivityBuyTicket", MODE_PRIVATE);
                        //preferencias.clearAll();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (resultCode == RESULT_CANCELED)
                    Toast.makeText(this, "Cancel", Toast.LENGTH_LONG).show();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
                Toast.makeText(this, "Ivalid", Toast.LENGTH_LONG).show();
        }
    }

    private String maskPrice(double value) {
        DecimalFormat df = new DecimalFormat("$#,##0.00");
        return df.format(value);
    }

    @Override
    protected void onStop() {
        super.onStop();
        payAmount.setText(null);
    }
}
