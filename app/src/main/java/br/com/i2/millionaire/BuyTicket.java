package br.com.i2.millionaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.consts.Const;
import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.pojo.Detail;
import br.com.i2.millionaire.utils.Util;

public class BuyTicket extends AppCompatActivity {
    private DatabaseReference databaseReference;

    private Toolbar toolbar;
    private EditText editBuyTicket;
    private Button actionbuy,detailTicket, buys;
    private ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_ticket);


        //Instantiating Layout objects bound to this class. The instance is performed according to the identifier of the object (id)
        toolbar = findViewById(R.id.top_toolbar);
        toolbar.setTitle("Select Ticket");
        actionbuy = findViewById(R.id.actionBuy);
        buys = findViewById(R.id.buys);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editBuyTicket = findViewById(R.id.edit_buy_ticket);

        //Button Buy
         buys.setOnClickListener(new View.OnClickListener() {

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(BuyTicket.this, TicketsBuys.class);
        startActivity(intent);
        finish();
    }
});

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                actionbuy.setEnabled(true);
                actionbuy.setBackgroundResource(R.drawable.botao_selecionado);
                return true;
            case R.id.item_sair:
                actionbuy.setEnabled(true);
                actionbuy.setBackgroundResource(R.drawable.botao_selecionado);
                Util.logout(this);
                return true;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    //Method for buy
    public void actionBuy(View view) {
        String buyNumber = editBuyTicket.getText().toString();
        if (buyNumber.isEmpty())
            Toast.makeText(BuyTicket.this, "O campo valor obrigatório!", Toast.LENGTH_LONG).show();
        else {
            databaseReference = ConfiguracaoFirebase.getFirebaseDataBase().child("tickets");
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String buyNumber = editBuyTicket.getText().toString();
                    Object child = dataSnapshot.child("tickets_resevados").child("index".concat(buyNumber)).child("tipo").getValue();
                    Object childAccumulated = dataSnapshot.child("accumulated").getValue();
                    Object child2 = dataSnapshot.child("tickets_soled").child("index".concat(buyNumber)).child("tipo").getValue();

                    if (child != null && String.valueOf(child).equals(String.valueOf(Const.Ticket.RESERVED_TICKET))) {
                        if(String.valueOf(child2).equals(String.valueOf(Const.Ticket.RESERVED_TICKET)))
                        {
                            Toast.makeText(BuyTicket.this, "O Tickets escolhido já foi vendido!", Toast.LENGTH_LONG).show();
                        }
                        else{
                        Toast.makeText(BuyTicket.this, "O Tickets escolhido já foi resevado!", Toast.LENGTH_LONG).show();
                        //actionbuy.setEnabled(false);
                        //actionbuy.setBackgroundResource(R.drawable.botao_selecionado2);
                    }
                        }
                    else if (childAccumulated != null && childAccumulated.equals("0"))
                        Toast.makeText(BuyTicket.this, "Não existe mais tickets para ser resevado!", Toast.LENGTH_LONG).show();
                    else {
                        reservedTicket(buyNumber);
                        adjustCounters(dataSnapshot);
                        reservedTicketByUser(buyNumber);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(BuyTicket.this, "Erro", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    //Method for reservd ticket
    private void reservedTicket(String buyNumber) {
        databaseReference.child("tickets_resevados").child("index".concat(buyNumber)).child("valor").setValue(buyNumber);
       databaseReference.child("tickets_resevados").child("index".concat(buyNumber)).child("tipo").setValue(Const.Ticket.RESERVED_TICKET);
        Toast.makeText(BuyTicket.this, "Ticket foi resevado.", Toast.LENGTH_LONG).show();
        editBuyTicket.setText(null);

        setSessionApp(buyNumber, String.valueOf(Const.Ticket.RESERVED_TICKET));
    }

    private void adjustCounters(DataSnapshot dataSnapshot) {
        String soled = dataSnapshot.child("soled").getValue().toString();
        String accumulated = dataSnapshot.child("accumulated").getValue().toString();

        int valueSoled = new Integer(soled) + 1;
        int valueAccumulated = new Integer(accumulated) - 1;

        databaseReference.child("soled").setValue(String.valueOf(valueSoled));
        databaseReference.child("accumulated").setValue(String.valueOf(valueAccumulated));
    }

    //Method for reserved ticket by user
    private void reservedTicketByUser(String buyNumber) {
        Preferencias preferencias = new Preferencias(BuyTicket.this);
        String idUser = preferencias.getValue(getString(R.string.id_user_app));

        databaseReference.child("tickets_por_usuario").child(idUser).child("".concat(buyNumber)).setValue(buyNumber);
        //databaseReference.child("tickets_por_usuario").child(idUser).child("index".concat(buyNumber)).child("tipo").setValue(Const.Ticket.RESERVED_TICKET);
    }


    private void setSessionApp(String tipo, String valor) {
      Preferencias preferencias = new Preferencias(BuyTicket.this, "sessionActivityBuyTicket", MODE_PRIVATE);
      preferencias.savePreferences(tipo, valor);
    }

    public void detailTicket(View view){
        Intent intent = new Intent(this, DetailsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }
}