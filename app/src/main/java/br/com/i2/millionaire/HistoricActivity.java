package br.com.i2.millionaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.tfb.fbtoast.FBToast;

import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.utils.Util;

public class HistoricActivity extends AppCompatActivity {

    //Create Objects
    private Toolbar toolbar;
    private DatabaseReference databaseReference;
    private TextView  txtTotalWinner, textAccountActive;
    private TextView tickectSoldValue, tickectCumulatedValue;
    private ValueEventListener valueEventListener;
    private TextView textLottery;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historic);

        //Cast in Toolbar
        toolbar = findViewById(R.id.top_toolbar);
        toolbar.setTitle("Historic");
        setSupportActionBar(toolbar);


        //Instantiating Layout objects bound to this class. The instance is performed according to the identifier of the object (id)
        tickectSoldValue = findViewById(R.id.text_tickect_sold_value);
        tickectCumulatedValue = findViewById(R.id.text_tickect_cumulated_value);
        //txtTotalLoterry = findViewById(R.id.txt_total_loterry);
        txtTotalWinner = findViewById(R.id.txt_total_winner);
        // txt_totalPrizeMoney = findViewById(R.id.txt_total_prize_money);
        textAccountActive = findViewById(R.id.textAccountActive);
        textLottery = findViewById(R.id.text_lottery);

        //Method for update TextViews
        updateTicket();



    }


//Methods for TextViews
    private void historic() {
        DatabaseReference databaseReference = ConfiguracaoFirebase.getFirebaseDataBase().child("tickets/historico");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //String qtdDinheiroPremio = dataSnapshot.child("qtd_dinheiro_premios").getValue().toString();
                String qtdGanhadores = dataSnapshot.child("qtd_ganhadores").getValue().toString();
                String qtdSorteios = dataSnapshot.child("qtd_sorteios").getValue().toString();

                //txtTotalLoterry.setText(qtdDinheiroPremio);
                txtTotalWinner.setText(qtdSorteios);
                //txt_totalPrizeMoney.setText(qtdSorteios);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {



            }
        });
    }

    //Verify account is valid (Boolean)
    private void isAccountValid() {
        String uid = new Preferencias(this).getValue(this.getString(R.string.id_user_app));
        DatabaseReference databaseReference = ConfiguracaoFirebase.getFirebaseDataBase().child("data_user").child(uid);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("accountChecked").getValue() == null)
                    textAccountActive.setVisibility(View.VISIBLE);
                else {
                    Boolean accountChecked = Boolean.parseBoolean(dataSnapshot.child("accountChecked").getValue().toString());
                    if (!accountChecked) textAccountActive.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Action of button Buy Ticket
    public void buyTicket(View view) {
        startActivity(new Intent(this, BuyTicket.class));
    }
    //Action of button Lottery
    public void lottery(View view) {
        startActivity(new Intent(this, MillionActivity.class));
    }
    //Action of button CheckAccount
    public void checkAccount(View view) {
        startActivity(new Intent(this, DataUser.class));
    }

    //Method of Button Back Native in Android( Present in device)
    public void onBackPressed() {

        Util.logout(this);


    }
    private void updateTicket() {
        databaseReference = ConfiguracaoFirebase.getFirebaseDataBase().child("tickets");

        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tickectSoldValue.setText(dataSnapshot.child("soled").getValue().toString());
                tickectCumulatedValue.setText(dataSnapshot.child("accumulated").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(HistoricActivity.this, "Erro", Toast.LENGTH_SHORT).show();
            }
        };
    }
    private void lottery() {
        databaseReference = databaseReference.child("tickets");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                textLottery.setText(dataSnapshot.child("chosen").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(HistoricActivity.this, "Sem Histórico", Toast.LENGTH_SHORT).show();
            }
        };
    }

    // Methods Override
    @Override
    protected void onStart() {

    super.onStart();
    isAccountValid();
    historic();
    databaseReference.addValueEventListener(valueEventListener);
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_sair:
                Util.logout(this);
                return true;
        }
        return true;
    }
    @Override
    protected void onStop() {
        super.onStop();
        databaseReference.removeEventListener(valueEventListener);
    }
}