package br.com.i2.millionaire;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.i2.millionaire.model.Ticket;

public class TicketsAdapter extends ArrayAdapter<Tickets> {
    private ArrayList<Tickets> arrayList;
    private Context context;

    public TicketsAdapter(@NonNull Context c, @NonNull ArrayList<Tickets> objects) {
        super(c, 0, objects);
        this.arrayList = objects;
        this.context = c;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = null;

        //Validando e criando a lista
        if (arrayList != null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            //Montando a view a partir do XML
            view = inflater.inflate(R.layout.activity_tickets_buys, parent, false);

            //Recuperando os componentes para exibição
            //TextView ticket = view.findViewById(R.id.tickets_user);
           //TextView ingredientes = view.findViewById(R.id.tv_ingredientes_lista);

            Tickets tickets = arrayList.get(position);
           // ticket.setText(tickets.getTicket());
           // ingredientes.setText(cardapio.getIngredientes());

        }
        return view;
    }
}