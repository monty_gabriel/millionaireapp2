package br.com.i2.millionaire.model;

public class Ticket {

    public Ticket() {
    }

    private String sold, accumulated;

    public String getSold() {
        return sold;
    }

    public String getAccumulated() {
        return accumulated;
    }
}
