package br.com.i2.millionaire;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.utils.Util;
import in.myinnos.androidscratchcard.ScratchCard;

public class MillionActivity extends AppCompatActivity {

    //Create Objects
    private Toolbar toolbar;
    private TextView textLottery;
    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_million);


        //Call Database
        databaseReference = new ConfiguracaoFirebase().getFirebaseDataBase();

        //Cast in Toolbar
        toolbar = findViewById(R.id.top_toolbar);
        toolbar.setTitle("Lottery");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textLottery = findViewById(R.id.text_lottery);


        //Call method Lottery
        lottery();
    }

    private void lottery() {
        databaseReference = databaseReference.child("tickets");
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                textLottery.setText(dataSnapshot.child("chosen").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }


 //Methods Override
    @Override
    public void onStart() {
        super.onStart();
        databaseReference.addValueEventListener(valueEventListener);



    }

    @Override
    public void onStop() {
        super.onStop();
        databaseReference.removeEventListener(valueEventListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.item_sair:
                Util.logout(this);
                return true;
        }
        return true;
    }
}