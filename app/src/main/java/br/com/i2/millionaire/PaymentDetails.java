package br.com.i2.millionaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.paypal.android.sdk.payments.PaymentActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.consts.Const;
import br.com.i2.millionaire.helper.Preferencias;
import br.com.i2.millionaire.utils.Util;

public class PaymentDetails extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView txtId, txtAmount, txtStatus;
    private DatabaseReference databaseReference;

    private List<Integer> listNumbers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        toolbar = findViewById(R.id.top_toolbar);
        toolbar.setTitle("Detalhes do Pagamento");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtId = findViewById(R.id.txtId);
        txtAmount = findViewById(R.id.txtAmount);
        txtStatus = findViewById(R.id.txtStatus);

        Intent intent = getIntent();

        Bundle bundle = getIntent().getExtras();

        listNumbers = (List<Integer>) bundle.get("listNumbers");

        try {
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetails(jsonObject.getJSONObject("response"), intent.getStringExtra("PayAmount"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.item_sair:
                Util.logout(this);
                return true;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    private void showDetails(JSONObject response, String paymentAmount) {
        try {
            txtId.setText(response.getString("id"));
            txtStatus.setText(response.getString("state"));
            txtAmount.setText("$" + paymentAmount);
            if (response.getString("state").equals("approved")) {
                databaseReference = ConfiguracaoFirebase.getFirebaseDataBase().child("tickets");
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Integer> numbers = listNumbers;
                        for (Integer number : numbers) {
                            String value = dataSnapshot.child("tickets_resevados")
                                    .child("index".concat(String.valueOf(number))).child("valor").getValue().toString();

                            if (!value.equals(null)) {
                                dataSnapshot.child("tickets_resevados")
                                        .child("index".concat(String.valueOf(number))).getRef().removeValue();

                               new Preferencias(PaymentDetails.this, "sessionActivityBuyTicket", MODE_PRIVATE).remove(value);

                                dataSnapshot.child("tickets_soled")
                                        .child("index".concat(String.valueOf(number))).getRef().setValue(value);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void voltar(View view){
        Intent intent = new Intent(this, HistoricActivity.class);
        startActivity(intent);
        finish();
    }
}