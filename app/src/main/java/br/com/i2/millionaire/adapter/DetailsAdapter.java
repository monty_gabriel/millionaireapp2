package br.com.i2.millionaire.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import br.com.i2.millionaire.Payment;
import br.com.i2.millionaire.R;
import br.com.i2.millionaire.config.ConfiguracaoFirebase;
import br.com.i2.millionaire.consts.Const;
import br.com.i2.millionaire.helper.Preferencias;

import br.com.i2.millionaire.pojo.Detail;

public class DetailsAdapter extends ArrayAdapter<Detail> {
    private final Activity activity;
    private Map<Integer, Double> mapQtdResevados;
    private Map<Integer, Double> mapQtdComprados;
    private List<Integer> listDeComprados;

    private TextView numberTicket, removeTicket, totalResevado, totalParaCompra;
    private CheckBox checkBoxBuy;
    private Button btnFinalizarCompra;

    private DatabaseReference databaseReference;

    public DetailsAdapter(Activity activity, List<Detail> details) {
        super(activity, 0, details);
        this.activity = activity;

        this.mapQtdResevados = new HashMap<>();
        this.mapQtdComprados = new HashMap<>();
        this.listDeComprados = new ArrayList<>();
    }

    static class ViewHolder {
        CheckBox checkBox;
        TextView textView;
    }

    @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

        final Detail detail = getItem(position);
        mapQtdResevados.put(position, Const.Currency.VALUE_TICKET);

        final ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_details, parent, false);

            totalParaCompra = activity.findViewById(R.id.total_para_compra);
            totalResevado = activity.findViewById(R.id.total_resevado);
            checkBoxBuy = convertView.findViewById(R.id.checkBox);

            numberTicket = convertView.findViewById(R.id.number_ticket);
            removeTicket = convertView.findViewById(R.id.remove_ticket);

            holder = new ViewHolder();
            holder.textView = removeTicket;
            holder.checkBox = checkBoxBuy;
            convertView.setTag(holder);

        } else
            holder = (ViewHolder) convertView.getTag();

        removeTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Alerta!");
                builder.setMessage("Confirma a remoção do ticket?");
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Boolean isDelete = new Preferencias(activity, "sessionActivityBuyTicket", activity.MODE_PRIVATE).remove(detail.getTicket());
                        if (isDelete) {
                            String uid = new Preferencias(activity).getValue(activity.getString(R.string.id_user_app));
                            databaseReference = ConfiguracaoFirebase.getFirebaseDataBase().child("tickets");
                            databaseReference.child("tickets_por_usuario").child(uid).child("index" + detail.getTicket()).setValue(null);
                            databaseReference.child("tickets_resevados").child("index" + detail.getTicket()).setValue(null);

                            if (listDeComprados.remove((Integer) Integer.parseInt(detail.getTicket()))) {
                                if (mapQtdComprados.containsKey(position)) {
                                    mapQtdComprados.remove(position);
                                    totalParaCompra.setText(maskPrice(somaTotal(mapQtdComprados)));
                                    holder.textView.setText("Removido");
                                    holder.textView.setTextColor(Color.RED);
                                    holder.checkBox.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                });

                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                builder.create().show();
            }
        });

        checkBoxBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mapQtdResevados.remove(position);
                    totalResevado.setText(maskPrice(somaTotal(mapQtdResevados)));

                    mapQtdComprados.put(position, Const.Currency.VALUE_TICKET);
                    totalParaCompra.setText(maskPrice(somaTotal(mapQtdComprados)));

                    listDeComprados.add(Integer.parseInt(detail.getTicket()));
                } else {
                    mapQtdResevados.put(position, Const.Currency.VALUE_TICKET);
                    totalResevado.setText(maskPrice(somaTotal(mapQtdResevados)));

                    mapQtdComprados.remove(position);
                    totalParaCompra.setText(maskPrice(somaTotal(mapQtdComprados)));

                    listDeComprados.remove((Integer) Integer.parseInt(detail.getTicket()));
                }

            }
        });

        btnFinalizarCompra = activity.findViewById(R.id.btn_finalizar_compra);
        btnFinalizarCompra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mapQtdComprados.size() == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle("Alerta!");
                    builder.setIcon(android.R.drawable.ic_dialog_alert);
                    builder.setMessage("Não existe ticket(s), para ser finalizados.");
                    builder.setPositiveButton("Fechar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
                } else {
                    Intent intent = new Intent(activity, Payment.class);
                    intent.putExtra("valorFinalCompra", totalParaCompra.getText().toString());
                    intent.putExtra("listNumbers", (ArrayList<Integer>) listDeComprados);
                    activity.startActivity(intent);
                    activity.finish();

                   
                }

            }
        });

        numberTicket.setText("Ticket ".concat(detail.getTicket()));
        totalResevado.setText(maskPrice(somaTotal(mapQtdResevados)));
        totalParaCompra.setText(maskPrice(0));
        return convertView;
    }

    private double somaTotal(Map<Integer, Double> map) {
        double cont = 0.0;
        for (Integer key : map.keySet()) cont += map.get(key).doubleValue();
        return cont;
    }

    private String maskPrice(double value) {
        DecimalFormat df = new DecimalFormat("$#,##0.00");
        return df.format(value);
    }
}
