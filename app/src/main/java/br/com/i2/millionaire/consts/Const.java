package br.com.i2.millionaire.consts;

public class Const {
    public static final String TAG_LOGIN_FIREBASE = "LOGIN_FIREBASE";
    public static final String TAG_LOGIN_GOOGLE = "LOGIN_GOOGLE";
    public static final String TAG_LOGIN_FACEBOOK = "LOGIN_FACEBOOK";
    public static final String LOG_CAT = "LOG";
    public static final int RC_SIGN_IN = 9001;

    public class Ticket {
        public static final int RESERVED_TICKET = 1;
        public static final int BUY_TICKET = 2;
        public static final int EXCLUDED_TICKET = 3;

        public static final String RESERVED = "RESEVADO";
        public static final String BUY = "COMPRADO";
        public static final String EXCLUDED = "EXCLUIDO";
    }

    public class Currency {
        public static final double VALUE_TICKET = 1.50;
    }
}